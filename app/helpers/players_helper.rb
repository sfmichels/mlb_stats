module PlayersHelper
  def birth_date_formatter(player)
    if player.birthYear.nil?
      "<unknown>"
    elsif player.birthMonth.nil? || player.birthDay.nil?
      player.birthYear.to_s
    else
      "#{player.birthMonth}/#{player.birthDay}/#{player.birthYear}"
    end
  end

  def death_date_formatter(player)
    if player.birthYear.nil?
      ""
    elsif player.deathMonth.nil? || player.deathDay.nil?
      player.deathYear.to_s
    else
      "#{player.deathMonth}/#{player.deathDay}/#{player.deathYear}"
    end
  end

  def decimal_display(value)
    if value.nil?
      ""
    else
      number_with_precision(value, precision: 3)
    end
  end

  def string_date(date)
    if date.nil?
      ""
    else
      date.strftime('%m/%d/%Y')
    end

  end
end
