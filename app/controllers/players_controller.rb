class PlayersController < ApplicationController
  def show
    @player = Player.find(params[:id])
    @batting = Batting.where(playerID: params[:id])
#    @team = Team.where(yearID: @batting.yearID, teamID: @batting.teamID, lgID: @batting.lgID)
#    sql = "SELECT b.*, t.name FROM Batting b JOIN Teams t ON t.yearID = b.yearID AND t.teamID = b.teamID AND t.lgID = b.lgID WHERE b.playerID = '#{params[:id]}' "
#    @batting = ActiveRecord::Base.connection.select_all(sql).to_hash
  end

  def search
    @players = Player.search(params[:nameFirst], params[:nameLast]).all
  end

  helper :all
end