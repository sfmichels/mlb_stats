class Batting < ActiveRecord::Base
  self.table_name = 'Batting'
  self.primary_keys = :playerID, :yearID, :stint

  # database column names are invalid variable names so we rename them
  alias_attribute 'double', '2B'
  alias_attribute 'triple', '3B'

  #belongs_to :player, :foreign_key => [:playerID, :yearID, :stint]
  has_one :player, :foreign_key => :playerID
  has_and_belongs_to_many :team, :class_name => 'Team'

  def add_calculated_values

  end

  def batting_average
    if self.AB.nil?
      nil
    else
      self.AB > 0 ? self.H.to_f / self.AB : 0.0
    end
  end

  def on_base_percentage
    if self.AB.nil?
      nil
    else
      self.AB > 0 ? [self.H.to_f, self.BB, self.HBP].compact.sum / [self.AB.to_f, self.BB, self.HBP, self.SF].compact.sum : 0.0
    end
  end

  def total_bases
    if self.H.nil?
      nil
    else
      self.H + self.double + 2 * self.triple + 3 * self.HR
    end
  end

  def slugging
    if self.AB.nil?
      nil
    else
      tb = total_bases
      puts tb
     tb / self.AB
    end
  end

end