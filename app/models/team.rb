class Team < ActiveRecord::Base
  self.table_name = 'Teams'
  self.primary_keys = [:yearID, :lgID, :teamID]

  has_and_belongs_to_many :batting, :class_name => 'Batting' #, :foreign_key => [:battingID, :yearID, :teamID]
end