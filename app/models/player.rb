class Player < ActiveRecord::Base
  self.table_name = 'Master'
  self.primary_key = 'playerID'

  has_many :batting, :class_name => 'Batting', :foreign_key => [:playerID, :yearID, :stint]

  BATS = { 'L' => 'Left', 'R' => 'Right', 'B' => 'Both' }
  THROWS = { 'L' => 'Left', 'R' => 'Right' }

  def self.search(search_nameFirst, search_nameLast)
    return scoped unless search_nameFirst.present? || search_nameLast.present?
    where(['nameFirst LIKE ? AND nameLast LIKE ?', "%#{search_nameFirst}%", "%#{search_nameLast}%"])
  end

  def to_s
    [nameFirst, nameLast].join(' ')
  end
end
