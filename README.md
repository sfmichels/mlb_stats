# MLB States
 
 Calculate baseball stats using the open source Baseball Databank.
 
 This application can currently create batting statistics for every player who has played in the major leagues.
 The database is mysql and is not Ruby On Rails friendly.
 
 * There are tables with primary keys that are strings.
 * There are composite primary keys.
 * Table names are not pluralized.
 * Attribute name use camel case like playerID, nameFirst and nameLast.
 
 The current state of the app can show batting stats for players but the future plans are to migrate the database to
 use rails conventions and then rewrite and expand the scope.
 
 This rails application is the work of [Alan Michels](mailto:sf.michels@gmail.com)
 
 The Baseball Databank is a separate open source project that can be found at: (www.baseball-databank.org).